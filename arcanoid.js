var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

var ballSize = 7;
var y = canvas.height-30;
var x = canvas.width/2;
var deltaX = 4;
var deltaY = -4;

var paddleHeight = 7;
var paddleWidth = 75;
var paddleX = (canvas.width-paddleWidth)/2;

var rightPressed = false;
var leftPressed = false;

var brickRowCount = 8;
var brickColumnCount = 10;
var brickWidth = 75;
var brickHeight = 20;
var brickPadding = 10;
var brickOffsetTop = 70;
var brickOffsetLeft = 30;

var score = 0;
var lives = 3;
var bricks = [];
var title = [];


for(var c=0; c<brickColumnCount; c++) {
    bricks[c] = [];
    for(var r=0; r<brickRowCount; r++) {
      bricks[c][r] = { x: 0, y: 0, status: 1 };
    }
  }
  
  document.addEventListener("keydown", keyDownHandler, false);
  document.addEventListener("keyup", keyUpHandler, false);

  function keyDownHandler(e) {
    if(e.keyCode == 39) {
      rightPressed = true;
    }
    else if(e.keyCode == 37) {
        leftPressed = true;
      }
    }

    function keyUpHandler(e) {
      if(e.keyCode == 39) {
      rightPressed = false;
    } else if(e.keyCode == 37) {
        leftPressed = false;
      }
    }

function collisionDetection() {

    for(var c=0; c<brickColumnCount; c++) {
        for(var r=0; r<brickRowCount; r++) {
          var b = bricks[c][r];
          if(b.status == 1) {
            if(x > b.x && x < b.x+brickWidth && y > b.y && y < b.y+brickHeight) {
              deltaY = -deltaY;
              b.status = -1;
              score++;
              if(score == brickRowCount*brickColumnCount) {
                alert("Congratulations!");
                document.location.reload();
              }
            }
        }
    }
}

}

function drawBall() {
  ctx.beginPath();
  ctx.arc(x, y, ballSize, 0, Math.PI*2);
  ctx.fillStyle = "#e8e8ef";
  ctx.fill();
  ctx.closePath();
}

function drawPaddle() {
    ctx.beginPath();
    ctx.rect(paddleX, canvas.height-paddleHeight, paddleWidth, paddleHeight);
    ctx.fillStyle = "#e8e8ef";
    ctx.fill();
    ctx.closePath();
}

function drawBricks() {

    for(var c=0; c<brickColumnCount; c++) {
        for(var r=0; r<brickRowCount; r++) {
          if(bricks[c][r].status == 1) {
            var brickX = (r*(brickWidth+brickPadding))+brickOffsetLeft;
            var brickY = (c*(brickHeight+brickPadding))+brickOffsetTop;
            bricks[c][r].x = brickX;
            bricks[c][r].y = brickY;
            ctx.beginPath();
            ctx.rect(brickX, brickY, brickWidth, brickHeight);
            ctx.fillStyle = "#4f0d0d";
            ctx.fill();
            ctx.closePath();
}
        }
    }
}


function drawScore() {

    ctx.font = "25px Times New Roman";
    ctx.fillStyle = "#e8e8ef";
    ctx.fillText("Score: "+score, 600, 30);

}

function drawTitle() {

    ctx.font = "40px OCR A Std";
    ctx.fillStyle = "#e8e8ef";
    ctx.fillText("Arkanoid"+title, 250, 50);

}

function drawLives() {

    ctx.font = "25px Times New Roman";
    ctx.fillStyle = "#e8e8ef";
    ctx.fillText("Lives: "+lives, 8, 30);

}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBricks();
    drawBall();
    drawPaddle();
    drawScore();
    drawLives();
    drawTitle();
    collisionDetection();

    if(x + deltaX > canvas.width-ballSize || x + deltaX < ballSize) {
        deltaX = -deltaX;
      }
      if(y + deltaY < ballSize) {
        deltaY = -deltaY;
      }
      else if(y + deltaY > canvas.height-ballSize) {
        if(x > paddleX && x < paddleX + paddleWidth) {
          deltaY = -deltaY;

        } 
        else {
            lives--;
            if(!lives) {
              alert("Try Again!");
              document.location.reload();
            } 
            else {
                x = canvas.width/2;
                y = canvas.height-30;
                deltaX = 4;
                deltaY = -4;
                paddleX = (canvas.width-paddleWidth)/2;
              }
    }
      } 
      
      if(rightPressed && paddleX < canvas.width-paddleWidth) {
        paddleX += 7;
      }
      else if(leftPressed && paddleX > 0) {
        paddleX -= 7;
      }
      x += deltaX;
    y += deltaY;
    requestAnimationFrame(draw);
      
    
    }

    draw();